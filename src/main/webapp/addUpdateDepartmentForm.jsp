<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <title>Departments </title>

    <!-- Responsive Meta Tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- main stylesheet -->

</head>
<body>
<form action="/controller" method="get" style="margin:auto;">
    <input type="hidden" name="command" value="goHome">
    <button type="submit" style="width:100%; background-color:rgba(0,0,0,0); border:none;">
        <p style="text-align: center; font-size: 0.8em; color: black;">
            <i class="fa fa-home fa-2x"></i>
        </p>
    </button>
</form>
<form action="/controller" method="post" style="margin-left: 25%; margin-top: 20px;">

    <c:if test="${errorList ne null}">
        <c:forEach items="${errorList}" var="errorList">
            <p style="color:red;">${errorList}</p>
        </c:forEach>
    </c:if>

    <c:if test="${department ne null}">
        <input type="hidden" name="command" value="updateDepartment">
        <input type="hidden" name="departmentId" value="${department.id}">
    </c:if>
    <c:if test="${department eq null}">
        <input type="hidden" name="command" value="addDepartment">
    </c:if><br><br>
    <input type="text" name="name" placeholder="Enter department name"
           <c:if test="${department ne null}">value="${department.name}" </c:if>> <br><br>
    <input type="text" name="address" placeholder="Enter address"
           <c:if test="${department ne null}">value="${department.address}" </c:if>><br><br>
    <input type="text" name="city" placeholder="Enter city"
           <c:if test="${department ne null}">value="${department.city}" </c:if>><br><br>
    <input type="text" name="country" placeholder="Enter country"
           <c:if test="${department ne null}">value="${department.country}" </c:if>><br><br>
    <input type="text" name="description" placeholder="Enter description"
           <c:if test="${department ne null}">value="${department.description}" </c:if>><br><br>

    <select id="employeeId" name="employeeId">

        <c:if test="${department ne null}">
            <c:if test="${department.managerId ne null}">
                <c:forEach items="${employees}" var="employees">
                    <c:if test="${employees.id == department.managerId}">
                        <option value="${employees.id}"> ${employees.lastName}&nbsp; ${employees.firstName}
                            &nbsp; ${employees.patronomic}</option>
                    </c:if>
                </c:forEach>

            </c:if>
        </c:if>

        <option value="null">Невыбрано</option>
        <c:forEach items="${employees}" var="employees">
            <option value="${employees.id}"> ${employees.lastName}&nbsp; ${employees.firstName}
                &nbsp; ${employees.patronomic}</option>
        </c:forEach>
    </select><br><br>


    <c:if test="${department ne null}">
        <br> <br><input class="inputChange" type="submit" value="Обновить"/>
    </c:if>
    <c:if test="${department eq null}">
        <br> <br> <input class="inputChange" type="submit" value="Добавить"/>
    </c:if>
</form>

</body>
</html>
