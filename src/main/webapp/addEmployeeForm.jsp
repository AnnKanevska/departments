<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="js/jquery.min.js"></script>
    <title>Employee form </title>

    <!-- Responsive Meta Tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- main stylesheet -->

<body>
<form action="/controller" method="get" style="margin:auto;">
    <input type="hidden" name="command" value="goHome">
    <button type="submit" style="width:100%; background-color:rgba(0,0,0,0); border:none;">
        <p style="text-align: center; font-size: 0.8em; color: black;">
            <i class="fa fa-home fa-2x"></i>
        </p>
    </button>
</form>
<form action="/controller" method="post" style="margin-left: 25%; margin-top: 20px;">

    <c:if test="${errorList ne null}">
        <c:forEach items="${errorList}" var="errorList">
            <p style="color:red;">${errorList}</p>
        </c:forEach>
    </c:if>
    <input type="hidden" name="command" value="updateEmployee">
    <input type="hidden" name="employeeId" value="${employee.id}">
    <input type="text" name="lastName" placeholder="Enter last name" value="${employee.lastName}" required>&nbsp;&nbsp;
    <input type="text" name="firstName" placeholder="Enter first name" value="${employee.firstName}" required>&nbsp;&nbsp;
    <input type="text" name="patronomic" placeholder="Enter patronomic" value="${employee.patronomic}" required>&nbsp;&nbsp;
    <br><br>
    <i class="fa fa-envelope-o">&nbsp;</i><input type="text" name="email" placeholder="Enter email"
                                                 value="${employee.email}" required>&nbsp;
    <i class="fa fa-birthday-cake">&nbsp;</i><input type="date" name="birthday" placeholder="Enter birthday"
                                                    value="${employee.birthday}" required>&nbsp;
    <i class="fa fa-phone">&nbsp;</i> <input type="text" name="phoneNumber" placeholder="Enter phone number"
                                             value="${employee.phoneNumber}" required>&nbsp;
    <i class="fa fa-money ">&nbsp;</i> <input type="text" name="salary" placeholder="Enter salary"
                                              value="${employee.salary}" required>&nbsp;
    <br>


    <p><input type="checkbox" id="isManager" onchange="setManager(this);" <c:if test="${employee.isManager ne null}">
              checked</c:if>> &nbsp; Менеджер </p>


    <i class="fa fa-building-o">&nbsp;</i>
    <select id="indepartment" name="department" <c:if test="${employee.isManager ne null}">disabled</c:if>>
        <c:if test="${employee.isManager eq null}">
            <c:if test="${employee.departmentName ne null}">
                <c:if test="${employee.isManager ne 'null'}">
                    <c:forEach items="${departments}" var="departments">
                        <c:if test="${departments.id == employee.departmentId}">
                            <option value="${employee.departmentId}"> ${departments.departmentName} </option>
                        </c:if>
                    </c:forEach>
                </c:if>
            </c:if>
        </c:if>
        <option value="null">Невыбрано</option>
        <c:forEach items="${departments}" var="departments">
            <option value="${departments.id}"> ${departments.departmentName} </option>
        </c:forEach>
    </select>

    <p style="color: red;">Менеджер отдела:
        <select id="manager" name="manager" <c:if test="${employee.isManager eq null}">disabled</c:if>>
            <c:if test="${employee.isManager ne null}">
                <c:if test="${employee.isManager ne 'null'}">
                    <c:forEach items="${departments}" var="departments">
                        <c:if test="${departments.id == employee.departmentId}">
                            <option value="${employee.departmentId}"> ${departments.departmentName} </option>
                        </c:if>
                    </c:forEach>
                </c:if>

            </c:if>
            <option value="null">Невыбрано</option>
            <c:forEach items="${departments}" var="departments">
                <option value="${departments.id}"> ${departments.departmentName} </option>
            </c:forEach>
        </select>
    </p>


    <input type="submit" value=" Update employee" class="addEmp">
</form>
<script>

    function setManager(sel) {
        var item1 = document.getElementById("indepartment");
        var item2 = document.getElementById("manager");
        if ($("#isManager").is(":checked")) {
            item1.value = "null";
            item1.disabled = true;
            item2.disabled = false;
        }

        else {
            item2.value = "null";
            item1.disabled = false;
            item2.disabled = true;
        }
    }
</script>
</body>
</html>
