<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="js/jquery.min.js"></script>
    <title>Departments </title>

    <!-- Responsive Meta Tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- main stylesheet -->

</head>
<body>
<button class="tablink" onclick="openPage('Department', this, 'red')" id="defaultOpen">Departments</button>
<button class="tablink" onclick="openPage('Employee', this, 'orange')">Employees</button>

<div id="Department" class="tabcontent">

    <form action="/controller" method="get" style="margin:auto;">
        <input type="hidden" name="command" value="goHome">
        <button type="submit" style="width:100%; background-color:rgba(0,0,0,0); border:none;">
            <p style="text-align: center; font-size: 0.8em; color: white;">
                <i class="fa fa-home fa-2x"></i>
            </p>
        </button>
    </form>

    <div class="exc">
        <div class="row">

            <div class="column">
                <form action="/controller" method="get">
                    <input type="hidden" name="command" value="createDepartmentForm">
                    <button class="content" type="submit" style="width:100%; height:22.3em">
                        <p style="height:3.5em; text-align: center; font-size: 1.8em">
                            <i class="fa fa-plus fa-5x"></i>
                        </p>
                    </button>
                </form>
            </div>


            <c:forEach items="${departments}" var="departments">
                <div class="column" idnumber= ${departments.id}>
                    <div class="content">

                        <h4><p style="text-align: center;"> ${departments.departmentName}</p>
                            <form action="/controller" method="get">
                                <input type="hidden" name="command" value="deleteDepartment">
                                <input type="hidden" name="departmentId" value="${departments.id}">
                                <button type="submit"
                                        style="margin-left:95%; margin-top:-60px; border: none; background-color: rgba(0,0,0,0);">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </form>
                        </h4>

                        <p style="width:100%; height: 5em">
                            <span class="params"><i class="fa fa-user-o fa-lg"> &nbsp;</i>
                            <c:if test="${departments.managerFullName ne null}">
                                    ${departments.managerFullName}
                            </c:if>
                                 <c:if test="${departments.managerFullName eq null}">
                                     ---------------
                                 </c:if>
                            </span>
                            <br>
                            <br>
                            <span class="params"><i class="fa fa-map-marker fa-lg"> &nbsp;</i>
                           ${departments.location.address}; ${departments.location.city}; ${departments.location.country.countryName}
                            </span>
                        </p>
                        <p style="height:5.5em;">
                                ${departments.description}
                        <form action="/controller" method="get">
                            <input type="hidden" name="command" value="editDepartmentForm">
                            <input type="hidden" name="departmentId" value="${departments.id}">
                            <input type="hidden" name="address" value="${departments.location.address}">
                            <input type="hidden" name="city" value="${departments.location.city}">
                            <input type="hidden" name="country" value="${departments.location.country.countryName}">
                            <input type="hidden" name="name" value="${departments.departmentName}">
                            <input type="hidden" name="description" value="${departments.description}">
                            <c:if test="${departments.managerId ne null}">
                                <input type="hidden" name="employeeId" value="${departments.managerId}">
                            </c:if>
                            <input class="inputChange" type="submit" value="Изменить"/>
                        </form>
                        <form action="/controller" method="get">
                            <input type="hidden" name="command" value="employeeList">
                            <input type="hidden" name="departmentId" value="${departments.id}">
                            <input type="submit" value="Список" class="inputList"/>
                        </form>
                        </p>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>


</div>


<div id="Employee" class="tabcontent">
    <form action="/controller" method="get" style="margin:auto;">
        <input type="hidden" name="command" value="goHome">
        <button type="submit" style="width:100%;background-color:rgba(0,0,0,0); border:none; ">
            <p style="text-align: center; font-size: 0.8em; color: white;">
                <i class="fa fa-home fa-2x"></i>
            </p>
        </button>
    </form>
    <div class="row">

        <div class="longColumn">
            <div class="content">
                <form action="/controller" method="post">

                    <c:if test="${errorList ne null}">
                        <c:forEach items="${errorList}" var="errorList">
                            <p style="color:red;">${errorList}</p>
                        </c:forEach>
                    </c:if>


                    <input type="hidden" name="command" value="addEmployee">
                    <input type="text" name="lastName" placeholder="Enter last name"
                           <c:if test="${employee ne null}">value="${employee.lastName}" </c:if>required>&nbsp;&nbsp;
                    <input type="text" name="firstName" placeholder="Enter first name"
                    <c:if test="${employee ne null}"> value="${employee.firstName}"</c:if> required>&nbsp;&nbsp;
                    <input type="text" name="patronomic" placeholder="Enter patronomic"
                    <c:if test="${employee ne null}"> value="${employee.patronomic}"</c:if> required>&nbsp;&nbsp;
                    <br><br>
                    <i class="fa fa-envelope-o">&nbsp;</i><input type="text" name="email"
                <c:if test="${employee ne null}"> value="${employee.email}" </c:if> placeholder="Enter email"
                                                                 required>&nbsp;
                    <i class="fa fa-birthday-cake">&nbsp;</i><input type="date" name="birthday"
                                                                    placeholder="Enter birthday"
                <c:if test="${employee ne null}"> value="${employee.birthday}"</c:if> required>&nbsp;
                    <i class="fa fa-phone">&nbsp;</i> <input type="text" name="phoneNumber"
                                                             placeholder="Enter phone number"
                <c:if test="${employee ne null}"> value="${employee.phoneNumber}"</c:if> required>&nbsp;
                    <i class="fa fa-money ">&nbsp;</i> <input type="text" name="salary"
                <c:if test="${employee ne null}"> value="${employee.salary}"</c:if> placeholder="Enter salary"
                                                              required>&nbsp;
                    <br>
                    <p><input type="checkbox" id="isManager" onchange="setManager(this);"
                    <c:if test="${employee ne null}">
                    <c:if test="${employee.isManager ne null}">
                              checked</c:if>
                    </c:if>> &nbsp; Менеджер </p>


                    <i class="fa fa-building-o">&nbsp;</i>
                    <select id="indepartment" name="department" <c:if test="${employee ne null}"> <c:if
                            test="${employee.isManager ne null}">disabled</c:if></c:if>>

                        <c:if test="${employee ne null}">
                            <c:if test="${employee.isManager eq null}">
                                <c:if test="${employee.departmentName ne null}">
                                    <c:if test="${employee.isManager ne 'null'}">
                                        <c:forEach items="${departments}" var="departments">
                                            <c:if test="${departments.id == employee.departmentId}">
                                                <option value="${employee.departmentId}"> ${departments.departmentName} </option>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </c:if>
                            </c:if>
                        </c:if>

                        <option value="null">Невыбрано</option>
                        <c:forEach items="${departments}" var="departments">
                            <option value="${departments.id}"> ${departments.departmentName} </option>
                        </c:forEach>
                    </select>

                    <p style="color: red;">Менеджер отдела:
                        <select id="manager" name="manager"
                                <c:if test="${employee eq null}">disabled</c:if>
                                <c:if test="${employee ne null}"> <c:if
                                        test="${employee.isManager eq null}">disabled</c:if></c:if>>
                            <c:if test="${employee ne null}">
                                <c:if test="${employee.isManager ne null}">
                                    <c:if test="${employee.isManager ne 'null'}">
                                        <c:forEach items="${departments}" var="departments">
                                            <c:if test="${departments.id == employee.departmentId}">
                                                <option value="${employee.departmentId}"> ${departments.departmentName} </option>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </c:if>
                            </c:if>

                            <option value="null">Невыбрано</option>
                            <c:forEach items="${departments}" var="departments">
                                <option value="${departments.id}"> ${departments.departmentName} </option>
                            </c:forEach>
                        </select>
                    </p>
                    <input type="submit" value=" Add employee" class="addEmp">

                </form>

            </div>
        </div>
        <c:forEach items="${employees}" var="employees">
            <div class="longColumn">
                <div class="content">
                    <c:if test="${employees.isManager ne null}">
                        <p><b>${employees.lastName} &nbsp; ${employees.firstName} &nbsp; ${employees.patronomic}</b>
                            &nbsp;<br><br>
                            <i class="fa fa-envelope-o">&nbsp;${employees.email}&nbsp;</i> &nbsp;
                            <i class="fa fa-birthday-cake">&nbsp;${employees.birthday}&nbsp;</i> &nbsp;
                            <i class="fa fa-phone">&nbsp;${employees.phoneNumber} &nbsp;</i> &nbsp;
                            <i class="fa fa-money ">&nbsp;${employees.salary} &nbsp;</i> &nbsp;
                            <i class="fa fa-building-o">&nbsp;</i><b style="color: red"> <i class="fa fa-user">
                                &nbsp;</i>Менеджер отдела: ${employees.isManager}</b>
                            <br>
                        <form action="/controller" method="get">
                            <input type="hidden" name="command" value="editEmployee">
                            <input type="hidden" name="employeeId" value="${employees.id}">
                            <input type="hidden" name="lastName" value="${employees.lastName}">
                            <input type="hidden" name="firstName" value="${employees.firstName}">
                            <input type="hidden" name="patronomic" value="${employees.patronomic}">
                            <input type="hidden" name="birthday" value="${employees.birthday}">
                            <input type="hidden" name="email" value="${employees.email}">
                            <input type="hidden" name="phoneNumber" value="${employees.phoneNumber}">
                            <input type="hidden" name="salary" value="${employees.salary}">
                            <input type="hidden" name="IsManager" value="${employees.isManager}">
                            <input type="hidden" name="departmentManagerId" value="${employees.departmentManagerId}">
                            <input class="updateEmp" type="submit" value="Изменить">
                        </form>

                        </p>
                    </c:if>
                    <c:if test="${employees.isManager eq null}">
                        <p><b>${employees.lastName} &nbsp; ${employees.firstName} &nbsp; ${employees.patronomic}</b>
                            &nbsp;
                        <form action="/controller" method="get">
                            <input type="hidden" name="command" value="deleteEmployee">
                            <input type="hidden" name="employeeId" value="${employees.id}">
                            <button type="submit"
                                    style="margin-left:98%; margin-top:-50px; border: none; background-color: rgba(0,0,0,0);">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </button>
                        </form>
                        <br>
                        <i class="fa fa-envelope-o">&nbsp;${employees.email}&nbsp;</i> &nbsp;
                        <i class="fa fa-birthday-cake">&nbsp;${employees.birthday}&nbsp;</i> &nbsp;
                        <i class="fa fa-phone">&nbsp;${employees.phoneNumber} &nbsp;</i> &nbsp;
                        <i class="fa fa-money ">&nbsp;${employees.salary} &nbsp;</i> &nbsp;

                        <i class="fa fa-building-o">&nbsp;
                            <c:if test="${employees.department.departmentName ne null}">
                                ${employees.department.departmentName}&nbsp;
                            </c:if>
                            <c:if test="${employees.department.departmentName eq null}">
                                -----------&nbsp;
                            </c:if>
                        </i> &nbsp;


                        <form action="/controller" method="get">
                            <input type="hidden" name="command" value="editEmployee">
                            <input type="hidden" name="employeeId" value="${employees.id}">
                            <input type="hidden" name="lastName" value="${employees.lastName}">
                            <input type="hidden" name="firstName" value="${employees.firstName}">
                            <input type="hidden" name="patronomic" value="${employees.patronomic}">
                            <input type="hidden" name="birthday" value="${employees.birthday}">
                            <input type="hidden" name="email" value="${employees.email}">
                            <input type="hidden" name="phoneNumber" value="${employees.phoneNumber}">
                            <input type="hidden" name="salary" value="${employees.salary}">
                            <input type="hidden" name="departmentId" value="${employees.department.id}">
                            <input type="hidden" name="departmentName" value="${employees.department.departmentName}">
                            <input type="submit" value="Изменить" class="updateEmp">
                        </form>

                        </p>
                    </c:if>

                </div>
            </div>
        </c:forEach>

    </div>
</div>

<script>
    function openPage(pageName, elmnt, color) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }
        document.getElementById(pageName).style.display = "block";
        elmnt.style.backgroundColor = color;

    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();


    function setManager(sel) {
        var item1 = document.getElementById("indepartment");
        var item2 = document.getElementById("manager");
        if ($("#isManager").is(":checked")) {
            item1.value = "null";
            item1.disabled = true;
            item2.disabled = false;
        }

        else {
            item2.value = "null";
            item1.disabled = false;
            item2.disabled = true;
        }
    }
</script>

</body>
</html>