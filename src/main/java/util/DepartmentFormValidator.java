package util;

import bean.DepartmentFormBean;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static metadata.ErrorMessage.ERR_INVALID_FORMAT;
import static metadata.RegExpConst.*;

public class DepartmentFormValidator implements Validator<DepartmentFormBean> {

    private ArrayList<String> errorMessages;
    private Pattern pattern;
    private Matcher matcher;

    public DepartmentFormValidator() {
        this.errorMessages = new ArrayList<>();
    }

    @Override
    public boolean isValid() {
        return errorMessages.isEmpty();
    }

    @Override
    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }

    @Override
    public void validate(DepartmentFormBean object) {
        validateCountry(object.getCountry());
        validateAddress(object.getAddress());
        validateName(object.getName());
        validateCity(object.getCity());

    }

    private void validateCountry(String country) {
        pattern = Pattern.compile(COUNTRY_REGEXP);
        matcher = pattern.matcher(country);
        if (!matcher.matches()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "country"));
            System.out.println("Country is incorrect");
        }
    }

    private void validateAddress(String address) {
        pattern = Pattern.compile(ADDRESS_REGEXP);
        matcher = pattern.matcher(address);
        if (!matcher.matches()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "address"));
            System.out.println("Address is incorrect");
        }
    }

    private void validateName(String name) {
        pattern = Pattern.compile(DEPARTMENT_NAME_REGEXP);
        matcher = pattern.matcher(name);
        if (!matcher.matches()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "name"));
            System.out.println("Name is incorrect");
        }
    }

    private void validateCity(String city) {
        pattern = Pattern.compile(CITY_REGEXP);
        matcher = pattern.matcher(city);
        if (!matcher.matches()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "city"));
            System.out.println("City is incorrect");
        }
    }

}
