package util;

import bean.EmployeeFormBean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static metadata.ErrorMessage.ERR_INVALID_FORMAT;
import static metadata.RegExpConst.*;


public class EmployeeFormValidator implements Validator<EmployeeFormBean> {

    private ArrayList<String> errorMessages;
    private Pattern pattern;
    private Matcher matcher;

    public EmployeeFormValidator() {
        this.errorMessages = new ArrayList<>();
    }

    @Override
    public boolean isValid() {
        return errorMessages.isEmpty();
    }

    @Override
    public ArrayList<String> getErrorMessages() {
        return errorMessages;
    }

    @Override
    public void validate(EmployeeFormBean object) {
        validateName(object.getLastName(), object.getFirstName(), object.getPatronomic());
        validateEmail(object.getEmail());
        validatePhone(object.getPhoneNumber());
        validateBirthday(object.getBirthday());
        validateSalary(object.getSalary());
    }

    private void validateEmail(String email) {
        pattern = Pattern.compile(EMAIL_REGEXP, Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(email);
        if (!matcher.matches()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "email"));
            System.out.println("Email is incorrect");
        }
    }

    private void validateName(String lastName, String firstName, String patronomic) {
        pattern = Pattern.compile(NAME_REGEXP);
        matcher = pattern.matcher(lastName);
        if (!matcher.matches()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "last name"));
            System.out.println("Lname is incorrect");
        }
        matcher = pattern.matcher(firstName);
        if (!matcher.matches()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "first name"));
            System.out.println("Fname is incorrect");
        }
        matcher = pattern.matcher(patronomic);
        if (!matcher.matches()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "patronomic"));
            System.out.println("Patronomic is incorrect");
        }
    }

    private void validatePhone(String phone) {
        pattern = Pattern.compile(PHONE_REGEXP);
        matcher = pattern.matcher(phone);
        if (!matcher.matches()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "phone"));
            System.out.println("Phone is incorrect");
        }
    }

    private void validateBirthday(String birthday) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = new Date();
        try {
            date = format.parse(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date.getTime() > (new Date()).getTime()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "birthday"));
            System.out.println("Birthday is incorrect");
        }
    }

    private void validateSalary(String salary) {
        pattern = Pattern.compile(SALARY_REGEXP);
        matcher = pattern.matcher(salary);
        if (!matcher.matches()) {
            errorMessages.add(String.format(ERR_INVALID_FORMAT, "salary"));
            System.out.println("Phone is incorrect");
        } else {
            if (Float.parseFloat(salary) < 0) {
                errorMessages.add(String.format(ERR_INVALID_FORMAT, "salary"));
                System.out.println("Salary is incorrect");
            }
        }
    }
}
