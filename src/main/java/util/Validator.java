package util;

import java.util.ArrayList;

public interface Validator<E> {

    boolean isValid();

    ArrayList<String> getErrorMessages();

    void validate(E object);

}
