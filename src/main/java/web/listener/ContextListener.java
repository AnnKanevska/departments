package web.listener;

import database.connectionFactory.ConnectionFactory;
import database.dao.DepartmentDao;
import database.dao.EmployeeDao;
import database.dao.daoFactory.DaoFactory;
import database.dao.daoFactory.MysqlDaoFactory;
import metadata.AppConst;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import service.DepartmentServiceImpl;
import service.EmployeeServiceImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ContextListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(ContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();
        log4jInit(context);
        DaoFactory daoFactory = new MysqlDaoFactory();
        initDepartmentService(context,daoFactory);
        initEmployeeService(context,daoFactory);
        LOGGER.debug("Context_init");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
    private void log4jInit(final ServletContext context) {
        InputStream inputStream = null;

        Properties props = new Properties();
        try {
            inputStream = context.getResourceAsStream("WEB-INF/log4j.properties");
            props.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PropertyConfigurator.configure(props);

    }


    private void initConnectionFactory(final ServletContext context) {
        ConnectionFactory connectionFactory = ConnectionFactory.getInstance();
        context.setAttribute(AppConst.CONNECTION_FACTORY, connectionFactory);
        //TODO: Logger
    }

    private void initDepartmentService(final ServletContext context, DaoFactory daoFactory) {
        DepartmentDao departmentDao = daoFactory.getDepartmentDao();
        context.setAttribute(AppConst.DEPARTMENT_DAO, departmentDao);

        DepartmentServiceImpl coursesService = new DepartmentServiceImpl(context);
        context.setAttribute(AppConst.DEPARTMENT_SERVICE, coursesService);

        //TODO: Logger
    }
    private void initEmployeeService(final ServletContext context, DaoFactory daoFactory) {

        EmployeeDao employeeDao = daoFactory.getEmployeeDao();
        context.setAttribute(AppConst.EMPLOYEE_DAO, employeeDao);

       EmployeeServiceImpl employeeService = new EmployeeServiceImpl(context);
        context.setAttribute(AppConst.EMPLOYEE_SERVICE, employeeService);

        //TODO: Logger
    }
}

