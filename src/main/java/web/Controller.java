package web;

import service.RequestHelper;
import web.command.Command;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {
    RequestHelper requestHelper;

    @Override
    public void init(ServletConfig conf) throws ServletException {
        super.init();
        requestHelper = RequestHelper.getInstance();

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        processRequest(req, resp);
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        throw new UnsupportedOperationException();
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String page = null;
        // definition of command from JSP;
        try {
            Command command = requestHelper.getCommand(req);
            // call method execute () from interface Command and send some params to handler-class
            //of this command
            page = command.execute(req, resp);

        } catch (ServletException e) {
            e.printStackTrace();
            //TODO:Logger


        }
        //call page-response
        if (page != null) {
            if (req.getAttribute("redirect") == null) {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher(page);
                requestDispatcher.forward(req, resp);
            } else {
                resp.sendRedirect(page);
                return;
            }
        }


    }


}
