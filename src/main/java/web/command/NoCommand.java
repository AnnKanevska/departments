package web.command;

import entity.Department;
import entity.Employee;
import metadata.AppConst;
import metadata.Path;
import service.DepartmentService;
import service.EmployeeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class NoCommand implements Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DepartmentService departmentService = (DepartmentService) request.getServletContext().getAttribute(AppConst.DEPARTMENT_SERVICE);
        EmployeeService employeeService = (EmployeeService) request.getServletContext().getAttribute(AppConst.EMPLOYEE_SERVICE);
        ArrayList<Department> departments = departmentService.getAllDepartments();
        ArrayList<Employee> employees = employeeService.getAllEmployees();
        request.setAttribute("employees", employees);
        request.setAttribute("departments", departments);
        return Path.MAIN_PAGE;
    }
}
