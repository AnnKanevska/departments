package web.command;

import metadata.AppConst;
import metadata.Path;
import service.EmployeeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteEmployeeFromDepartment implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EmployeeService employeeService = (EmployeeService) request.getServletContext().getAttribute(AppConst.EMPLOYEE_SERVICE);
        employeeService.deleteEmployeeFromDepartment(Integer.parseInt(request.getParameter("employeeId")));

        request.setAttribute("redirect", "true");
        return Path.INDEX_PAGE;
    }
}
