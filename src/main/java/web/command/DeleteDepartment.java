package web.command;

import metadata.AppConst;
import metadata.Path;
import service.DepartmentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteDepartment implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DepartmentService departmentService = (DepartmentService) request.getServletContext().getAttribute(AppConst.DEPARTMENT_SERVICE);

        departmentService.deleteDepartmentById(Integer.parseInt(request.getParameter("departmentId")));

        request.setAttribute("redirect", "true");
        return Path.INDEX_PAGE;
    }
}
