package web.command;

import entity.Employee;
import metadata.AppConst;
import metadata.Path;
import service.EmployeeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class СreateDepartmentForm implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        EmployeeService employeeService = (EmployeeService) request.getServletContext().getAttribute(AppConst.EMPLOYEE_SERVICE);

        ArrayList<Employee> employees = employeeService.getAllEmployees();
        request.setAttribute("employees", employees);

        return Path.ADD_DEPARTMENT_FORM;
    }
}
