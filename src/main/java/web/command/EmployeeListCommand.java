package web.command;

import entity.Department;
import entity.Employee;
import metadata.AppConst;
import metadata.Path;
import service.DepartmentService;
import service.EmployeeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class EmployeeListCommand implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DepartmentService departmentService = (DepartmentService) request.getServletContext().getAttribute(AppConst.DEPARTMENT_SERVICE);
        EmployeeService employeeService = (EmployeeService) request.getServletContext().getAttribute(AppConst.EMPLOYEE_SERVICE);
        ArrayList<Employee> employees = employeeService.getEmployeesByDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        ArrayList<Department> departments = departmentService.getAllDepartments();
        request.setAttribute("departments", departments);
        request.setAttribute("employees", employees);
        request.setAttribute("departmentId", Integer.parseInt(request.getParameter("departmentId")));
        return Path.EMPLOYEE_LIST_PAGE;
    }
}
