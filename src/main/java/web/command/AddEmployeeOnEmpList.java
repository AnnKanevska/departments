package web.command;

import bean.EmployeeFormBean;
import entity.Department;
import entity.Employee;
import metadata.AppConst;
import metadata.Path;
import service.DepartmentService;
import service.EmployeeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static bean.EmployeeFormBean.setEmployeeFormBean;

public class AddEmployeeOnEmpList implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DepartmentService departmentService = (DepartmentService) request.getServletContext().getAttribute(AppConst.DEPARTMENT_SERVICE);
        EmployeeService employeeService = (EmployeeService) request.getServletContext().getAttribute(AppConst.EMPLOYEE_SERVICE);

        EmployeeFormBean employee = setEmployeeFormBean(request);
        request.setAttribute("employee", employee);

        ArrayList<String> errorList = employeeService.insertEmployee(employee);
        request.setAttribute("errorList", errorList);

        ArrayList<Department> departments = departmentService.getAllDepartments();
        request.setAttribute("departments", departments);

        ArrayList<Employee> employees = employeeService.getEmployeesByDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

        request.setAttribute("employees", employees);
        request.setAttribute("departmentId", Integer.parseInt(request.getParameter("departmentId")));

        return Path.EMPLOYEE_LIST_PAGE;
    }
}
