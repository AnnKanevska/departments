package web.command;

import bean.EmployeeFormBean;
import entity.Department;
import metadata.AppConst;
import metadata.Path;
import service.DepartmentService;
import service.EmployeeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static bean.EmployeeFormBean.setEmployeeFormBean;

public class UpdateEmployee implements Command {
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DepartmentService departmentService = (DepartmentService) request.getServletContext().getAttribute(AppConst.DEPARTMENT_SERVICE);
        EmployeeService employeeService = (EmployeeService) request.getServletContext().getAttribute(AppConst.EMPLOYEE_SERVICE);

        EmployeeFormBean employee = setEmployeeFormBean(request);
        request.setAttribute("employee", employee);

        ArrayList<String> errorList = employeeService.addEmployee(employee);
        request.setAttribute("errorList", errorList);

        ArrayList<Department> departments = departmentService.getAllDepartments();
        request.setAttribute("departments", departments);

        return Path.ADD_EMPLOYEE_FORM;
    }
}
