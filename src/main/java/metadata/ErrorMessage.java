package metadata;

public final class ErrorMessage {
    public static final String ERROR_MESSAGE = "error_message";
    public static final String ERROR_MESSAGES = "error_messages";

    public static final String ERR_EMP_EXIST = "Employee with email %s exists";
    public static final String ERR_EMP_NOT_EXIST = "Employee with email %s not exists";
    public static final String ERR_UNFILLED_INPUT_FIELDS = "Please fill all empty fields";
    public static final String ERR_INVALID_FORMAT = "Invalid %s";

    public static final String ERR_DEP_EXIST = "Department with name %s exists";
}
