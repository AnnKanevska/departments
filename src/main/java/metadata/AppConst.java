package metadata;

public final class AppConst {

    public static final String CONNECTION_FACTORY = "connection factory";

    public static final String DEPARTMENT_DAO = "department dao";
    public static final String EMPLOYEE_DAO = "employee_dao";

    public static final String DEPARTMENT_SERVICE = "department_service";
    public static final String EMPLOYEE_SERVICE = "employee_service";
}
