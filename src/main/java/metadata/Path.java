package metadata;

public class Path {

    public static final String MAIN_PAGE = "main.jsp";
    public static final String ERROR_PAGE = "error.jsp";
    public static final String INDEX_PAGE = "index.jsp";
    public static final String EMPLOYEE_LIST_PAGE = "employeeList.jsp";
    public static final String ADD_EMPLOYEE_FORM = "addEmployeeForm.jsp";
    public static final String ADD_DEPARTMENT_FORM = "addUpdateDepartmentForm.jsp";

}
