package metadata;

public class DBMetadata {

    public static final String DATABASE_NAME = "department";

    public abstract class TablesNames {

        public static final String COUNTRIES = "countries";
        public static final String LOCATIONS = "locations";
        public static final String DEPARTMENTS = "departments";
        public static final String EMPLOYEES = "employees";

    }

    public abstract class CountriesMetadata {

        public static final String COUNTRY_ID = TablesNames.COUNTRIES + "." + "country_id";
        public static final String COUNTRY_ID_WITHOUT = "country_id";
        public static final String COUNTRY_NAME = TablesNames.COUNTRIES + "." + "country_name";
        public static final String COUNTRY_NAME_WITHOUT = "country_name";
    }

    public abstract class LocationsMetadata {

        public static final String LOCATION_ID = TablesNames.LOCATIONS + "." + "location_id";
        public static final String ADDRESS = TablesNames.LOCATIONS + "." + "address";
        public static final String CITY = TablesNames.LOCATIONS + "." + "city";
        public static final String COUNTRY_ID = TablesNames.LOCATIONS + "." + "country_id";

    }

    public abstract class DepartmentsMetadata {

        public static final String DEPARTMENT_ID = TablesNames.DEPARTMENTS + "." + "department_id";
        public static final String DEPARTMENT_NAME = TablesNames.DEPARTMENTS + "." + "department_name";
        public static final String DESCRIPTION = TablesNames.DEPARTMENTS + "." + "description";
        public static final String LOCATION_ID = TablesNames.DEPARTMENTS + "." + "location_id";
        public static final String MANAGER_ID = TablesNames.DEPARTMENTS + "." + "manager_id";
        public static final String DEPARTMENT_ID_WITHOUT = "department_id";
        public static final String DEPARTMENT_NAME_WITHOUT = "department_name";
        public static final String DESCRIPTION_WITHOUT = "description";
        public static final String LOCATION_ID_WITHOUT = "location_id";
        public static final String MANAGER_ID_WITHOUT = "manager_id";

    }

    public abstract class EmployeesMetadata {

        public static final String EMPLOYEE_ID = TablesNames.EMPLOYEES + "." + "employee_id";
        public static final String EMPLOYEE_ID_WITHOUT = "employee_id";
        public static final String FIRST_NAME_WITHOUT = "first_name";
        public static final String FIRST_NAME = TablesNames.EMPLOYEES + "." + "first_name";
        public static final String LAST_NAME_WITHOUT = "last_name";
        public static final String LAST_NAME = TablesNames.EMPLOYEES + "." + "last_name";
        public static final String PATRONOMIC_WITHOUT = "patronomic";
        public static final String PATRONOMIC = TablesNames.EMPLOYEES + "." + "patronomic";
        public static final String EMAIL_WITHOUT = "email";
        public static final String EMAIL = TablesNames.EMPLOYEES + "." + "email";
        public static final String BIRTHDAY_WITHOUT = "birthday";
        public static final String BIRTHDAY = TablesNames.EMPLOYEES + "." + "birthday";
        public static final String PHONE_NUMBER_WITHOUT = "phone_number";
        public static final String PHONE_NUMBER = TablesNames.EMPLOYEES + "." + "phone_number";
        public static final String SALARY_WITHOUT = "salary";
        public static final String SALARY = TablesNames.EMPLOYEES + "." + "salary";
        public static final String DEPARTMENT_ID_WITHOUT = "department_id";
        public static final String DEPARTMENT_ID = TablesNames.EMPLOYEES + "." + "department_id";

    }
}
