package service;

import bean.DepartmentFormBean;
import entity.Department;

import java.util.ArrayList;

public interface DepartmentService {

    ArrayList<Department> getAllDepartments();

    void deleteDepartmentById(int departmentId);

    void deleteManagerFromDepartment(Integer departmentId);

    ArrayList<String> updateDepartment(DepartmentFormBean departmentFormBean);

    ArrayList<String> addDepartment(DepartmentFormBean departmentFormBean);

}
