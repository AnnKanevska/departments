package service;

import bean.EmployeeFormBean;
import database.dao.EmployeeDao;
import entity.Employee;
import metadata.AppConst;
import metadata.ErrorMessage;
import util.EmployeeFormValidator;
import util.Validator;

import javax.servlet.ServletContext;
import java.util.ArrayList;

public class EmployeeServiceImpl implements EmployeeService {
    private Validator<EmployeeFormBean> validator;
    private final EmployeeDao employeeDao;

    public EmployeeServiceImpl(final ServletContext context) {
        this.employeeDao = (EmployeeDao) context.getAttribute(AppConst.EMPLOYEE_DAO);
    }

    @Override
    public ArrayList<Employee> getEmployeesByDepartmentId(int departmentId) {
        return employeeDao.getAllEmployeesByDepartmentId(departmentId);
    }

    @Override
    public ArrayList<Employee> getAllEmployees() {
        return employeeDao.getAllEmployees();
    }

    @Override
    public void deleteEmployeeById(Integer employeeId) {
        employeeDao.deleteEmployeeById(employeeId);
    }

    @Override
    public ArrayList<String> addEmployee(EmployeeFormBean employeeFormBean) {
        validator = new EmployeeFormValidator();
        validator.validate(employeeFormBean);
        if (validator.isValid()) {
            ArrayList<String> successList = new ArrayList();
            if (employeeDao.isExistEmployeeWithEmail(employeeFormBean.getEmail())) {
                if (employeeDao.getExistEmployeeId(employeeFormBean.getEmail()) != employeeFormBean.getId()) {
                    successList.add(String.format(ErrorMessage.ERR_EMP_EXIST, employeeFormBean.getEmail()));
                    return successList;
                }
            }
            employeeDao.addEmployee(employeeFormBean);

            successList.add("Success!");
            return successList;
        } else {
            return validator.getErrorMessages();
        }
    }

    @Override
    public ArrayList<String> insertEmployee(EmployeeFormBean employeeFormBean) {
        validator = new EmployeeFormValidator();
        validator.validate(employeeFormBean);
        if (validator.isValid()) {
            ArrayList<String> successList = new ArrayList();
            if (employeeDao.isExistEmployeeWithEmail(employeeFormBean.getEmail())) {
                successList.add(String.format(ErrorMessage.ERR_EMP_EXIST, employeeFormBean.getEmail()));
                return successList;
            }
            employeeDao.insertEmployee(employeeFormBean);

            successList.add("Success!");
            return successList;
        } else {
            return validator.getErrorMessages();
        }
    }

    @Override
    public void deleteEmployeeFromDepartment(Integer employeeId) {
        employeeDao.removeEmployeeFromDepartment(employeeId);
    }

    @Override
    public void deleteManagerFromDepartment(Integer employeeId) {
        employeeDao.removeManagerFromDepartment(employeeId);
    }


}
