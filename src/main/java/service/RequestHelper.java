package service;


import web.command.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

public class RequestHelper {
    private static RequestHelper instance = null;
    HashMap<String, Command> commands = new HashMap<>();

    private RequestHelper() {
        commands.put("employeeList", new EmployeeListCommand());
        commands.put("deleteEmployee", new DeleteEmployee());
        commands.put("editEmployee", new EditEmployee());
        commands.put("updateEmployee", new UpdateEmployee());
        commands.put("addEmployee", new AddEmployee());
        commands.put("deleteManagerFromDepartment", new DeleteManagerFromDepartment());
        commands.put("deleteEmployeeFromDepartment", new DeleteEmployeeFromDepartment());

        commands.put("editDepartmentForm", new EditDepartmentForm());
        commands.put("updateDepartment", new UpdateDepartment());
        commands.put("addDepartment", new AddDepartment());
        commands.put("deleteDepartment", new DeleteDepartment());
        commands.put("createDepartmentForm", new СreateDepartmentForm());
        commands.put("addEmployeeOnEmpList", new AddEmployeeOnEmpList());

                commands.put("gohome", new GoHome());

    }

    public Command getCommand(HttpServletRequest request) {
        String action = request.getParameter("command");
        Command command = commands.get(action);

        if (command == null) {
            command = new NoCommand();
        }
        return command;
    }


    public static RequestHelper getInstance() {
        if (instance == null) {
            instance = new RequestHelper();
        }
        return instance;
    }
}
