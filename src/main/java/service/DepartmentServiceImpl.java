package service;

import bean.DepartmentFormBean;
import database.dao.DepartmentDao;
import entity.Department;
import metadata.AppConst;
import metadata.ErrorMessage;
import util.DepartmentFormValidator;
import util.Validator;

import javax.servlet.ServletContext;
import java.util.ArrayList;

public class DepartmentServiceImpl implements DepartmentService {
    private Validator<DepartmentFormBean> validator;
    private final DepartmentDao departmentDao;

    public DepartmentServiceImpl(final ServletContext context) {
        this.departmentDao = (DepartmentDao) context.getAttribute(AppConst.DEPARTMENT_DAO);
    }


    @Override
    public ArrayList<Department> getAllDepartments() {
        return departmentDao.getAllDepartments();
    }

    @Override
    public void deleteDepartmentById(int departmentId) {
        departmentDao.deleteDepartment(departmentId);
    }

    @Override
    public void deleteManagerFromDepartment(Integer departmentId) {
        departmentDao.removeManagerByDepartmentId(departmentId);
    }

    @Override
    public ArrayList<String> updateDepartment(DepartmentFormBean departmentFormBean) {

        validator = new DepartmentFormValidator();
        validator.validate(departmentFormBean);

        if (validator.isValid()) {
            ArrayList<String> successList = new ArrayList();
            if (departmentDao.isExistDepartmentWithName(departmentFormBean.getName())) {
                if (departmentDao.getExistDepartmentId(departmentFormBean.getName())!=departmentFormBean.getId()) {
                    successList.add(String.format(ErrorMessage.ERR_DEP_EXIST, departmentFormBean.getName()));
                    return successList;
                }
            }
            departmentDao.updateDepartment(departmentFormBean);

            successList.add("Success!");
            return successList;
        } else {
            return validator.getErrorMessages();
        }
    }

    @Override
    public ArrayList<String> addDepartment(DepartmentFormBean departmentFormBean) {
        validator = new DepartmentFormValidator();
        validator.validate(departmentFormBean);
        if (validator.isValid()) {
            ArrayList<String> successList = new ArrayList();
            if (departmentDao.isExistDepartmentWithName(departmentFormBean.getName())) {
                successList.add(String.format(ErrorMessage.ERR_DEP_EXIST, departmentFormBean.getName()));
                return successList;
            }
            departmentDao.addDepartment(departmentFormBean);

            successList.add("Success!");
            return successList;
        } else {
            return validator.getErrorMessages();
        }
    }
}
