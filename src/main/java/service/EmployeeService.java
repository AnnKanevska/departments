package service;

import bean.EmployeeFormBean;
import entity.Employee;

import java.util.ArrayList;

public interface EmployeeService {

    ArrayList<Employee> getEmployeesByDepartmentId(int departmentId);

    ArrayList<Employee> getAllEmployees();

    void deleteEmployeeById(Integer employeeId);

    ArrayList<String> addEmployee(EmployeeFormBean employeeFormBean);

    ArrayList<String> insertEmployee(EmployeeFormBean employeeFormBean);

    void deleteEmployeeFromDepartment(Integer employeeId);

    void deleteManagerFromDepartment(Integer employeeId);

}
