package database.dao.impl;

import bean.EmployeeFormBean;
import database.dao.EmployeeDao;
import database.dao.util.QueryBuilder;
import entity.Employee;
import metadata.DBMetadata.DepartmentsMetadata;
import metadata.DBMetadata.EmployeesMetadata;
import metadata.DBMetadata.TablesNames;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class EmployeeDaoImpl extends BaseDaoWrapper<Employee> implements EmployeeDao {

    private void createQueryForAllEmployee(QueryBuilder qb) {
        qb.addTable(TablesNames.DEPARTMENTS, TablesNames.EMPLOYEES);
        qb.addWhereParams(qb.setQuartletFourParams(EmployeesMetadata.DEPARTMENT_ID, QueryBuilder.Sign.EQUAL, DepartmentsMetadata.DEPARTMENT_ID, QueryBuilder.Operators.AND));
    }


    //SELECT * FROM employees,departments WHERE employees.employee_id = departments.manager_id AND departments.department_id=1
    // UNION
    // SELECT *FROM employees,departments WHERE employees.department_id = departments.department_id AND departments.department_id=1;

    public ArrayList<Employee> getAllEmployeesByDepartmentId(Integer departmentId) {
        QueryBuilder qb = new QueryBuilder();
        StringBuffer sb = new StringBuffer();
        createQueryForAllEmployee(qb);
        qb.addWhereParams(qb.setQuartletTwoParams(DepartmentsMetadata.DEPARTMENT_ID, QueryBuilder.Sign.EQUAL));
        sb.append(qb.buildSelectRequest().substring(0, qb.buildSelectRequest().length() - 1));
        sb.append(" UNION ");
        qb = new QueryBuilder();
        qb.addTable(TablesNames.DEPARTMENTS, TablesNames.EMPLOYEES);
        qb.addWhereParams(qb.setQuartletFourParams(EmployeesMetadata.EMPLOYEE_ID, QueryBuilder.Sign.EQUAL, DepartmentsMetadata.MANAGER_ID, QueryBuilder.Operators.AND),
                qb.setQuartletTwoParams(DepartmentsMetadata.DEPARTMENT_ID, QueryBuilder.Sign.EQUAL));
        sb.append(qb.buildSelectRequest());
        return getByQueryAndParams(sb.toString(), departmentId, departmentId);
    }

    //  SELECT emp.*,dep.managerDepName FROM
    // SELECT employees.*,departments.department_id AS 'departmentId', departments.department_name AS 'departmentName'
    // FROM employees LEFT JOIN departments ON employees.department_id = departments.department_id)emp
    // LEFT JOIN(SELECT departments.department_name AS 'managerDepName', employees.employee_id  FROM employees
    // LEFT JOIN departments ON employees.employee_id = departments.manager_id)dep ON emp.employee_id =dep.employee_id;


    public ArrayList<Employee> getAllEmployees() {
        QueryBuilder qb = new QueryBuilder();
        StringBuffer sb = new StringBuffer();
        qb.addColumn("employees.*", DepartmentsMetadata.DEPARTMENT_ID + " AS 'departmentId'", DepartmentsMetadata.DEPARTMENT_NAME + " AS 'departmentName'");
        qb.addTable(TablesNames.EMPLOYEES);
        qb.addLeftJoinParams(TablesNames.DEPARTMENTS, EmployeesMetadata.DEPARTMENT_ID, DepartmentsMetadata.DEPARTMENT_ID);
        String fromParam = qb.buildSelectRequest().substring(0, qb.buildSelectRequest().length() - 1);

        qb = new QueryBuilder();
        qb.addColumn(DepartmentsMetadata.DEPARTMENT_NAME + " AS 'managerDepName'", DepartmentsMetadata.DEPARTMENT_ID + " AS 'managerDepId'", EmployeesMetadata.EMPLOYEE_ID);
        qb.addTable(TablesNames.EMPLOYEES);
        qb.addLeftJoinParams(TablesNames.DEPARTMENTS, EmployeesMetadata.EMPLOYEE_ID, DepartmentsMetadata.MANAGER_ID);
        String joinParam = qb.buildSelectRequest().substring(0, qb.buildSelectRequest().length() - 1);

        qb = new QueryBuilder();
        qb.addColumn("emp.*", "dep.managerDepName", "dep.managerDepId");
        qb.addTable("(" + fromParam + ") emp ");
        qb.addLeftJoinParams(" (" + joinParam + ") dep ", "emp.employee_id", "dep.employee_id");
        return getByQuery(qb.buildSelectRequest());
    }

    @Override
    public void deleteEmployeeById(Integer employeeId) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.EMPLOYEES);
        qb.addWhereParams(qb.setQuartletThreeParams(EmployeesMetadata.EMPLOYEE_ID, QueryBuilder.Sign.EQUAL, employeeId + ""));
        addUpdateEntity(qb.buildDeleteRequest());
    }

    @Override
    public void addEmployee(EmployeeFormBean employeeFormBean) {
        LinkedHashMap<String, String> values = new LinkedHashMap<>();
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.EMPLOYEES);
        qb.addWhereParams(qb.setQuartletThreeParams(EmployeesMetadata.EMPLOYEE_ID, QueryBuilder.Sign.EQUAL, employeeFormBean.getId() + ""));

        values.put(EmployeesMetadata.FIRST_NAME, "'" + employeeFormBean.getFirstName() + "'");
        values.put(EmployeesMetadata.LAST_NAME, "'" + employeeFormBean.getLastName() + "'");
        values.put(EmployeesMetadata.PATRONOMIC, "'" + employeeFormBean.getPatronomic() + "'");
        values.put(EmployeesMetadata.BIRTHDAY, "'" + employeeFormBean.getBirthday() + "'");
        values.put(EmployeesMetadata.EMAIL, "'" + employeeFormBean.getEmail() + "'");
        values.put(EmployeesMetadata.SALARY, employeeFormBean.getSalary() + "");
        values.put(EmployeesMetadata.PHONE_NUMBER, "'" + employeeFormBean.getPhoneNumber() + "'");
        if (employeeFormBean.getIsManager() == null) {


            LinkedHashMap<String, String> va = new LinkedHashMap<>();
            QueryBuilder qb1 = new QueryBuilder();
            qb1.addTable(TablesNames.DEPARTMENTS);
            qb1.addWhereParams(qb.setQuartletThreeParams(DepartmentsMetadata.MANAGER_ID, QueryBuilder.Sign.EQUAL, employeeFormBean.getId() + ""));
            va.put(DepartmentsMetadata.MANAGER_ID, " NULL");
            addUpdateEntity(qb1.buildUpdateRequest(va));

            values.put(EmployeesMetadata.DEPARTMENT_ID, employeeFormBean.getDepartmentId() + "");
            addUpdateEntity(qb.buildUpdateRequest(values));
        } else {
            values.put(EmployeesMetadata.DEPARTMENT_ID, " NULL ");
            addUpdateEntity(qb.buildUpdateRequest(values));
            qb = new QueryBuilder();
            qb.addTable(TablesNames.DEPARTMENTS);
            qb.addWhereParams(qb.setQuartletThreeParams(DepartmentsMetadata.DEPARTMENT_ID, QueryBuilder.Sign.EQUAL, employeeFormBean.getDepartmentId() + ""));
            values = new LinkedHashMap<>();
            values.put(DepartmentsMetadata.MANAGER_ID, employeeFormBean.getId() + "");
            addUpdateEntity(qb.buildUpdateRequest(values));
        }
    }

    @Override
    public boolean isExistEmployeeWithEmail(String email) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.EMPLOYEES);
        qb.addWhereParams(qb.setQuartletTwoParams(EmployeesMetadata.EMAIL, QueryBuilder.Sign.EQUAL));

        return !getByQueryAndParams(qb.buildSelectRequest(), email).isEmpty();
    }

    @Override
    public Integer getExistEmployeeId(String email) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.EMPLOYEES);
        qb.addWhereParams(qb.setQuartletTwoParams(EmployeesMetadata.EMAIL, QueryBuilder.Sign.EQUAL));

        return getByQueryAndParams(qb.buildSelectRequest(), email).get(0).getId();
    }

    @Override
    public void insertEmployee(EmployeeFormBean employeeFormBean) {
        StringBuffer values = new StringBuffer();
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.EMPLOYEES);
        qb.addColumn(EmployeesMetadata.FIRST_NAME, EmployeesMetadata.LAST_NAME, EmployeesMetadata.PATRONOMIC, EmployeesMetadata.BIRTHDAY, EmployeesMetadata.EMAIL, EmployeesMetadata.SALARY, EmployeesMetadata.PHONE_NUMBER);
        qb.addColumn(EmployeesMetadata.DEPARTMENT_ID);

        values.append("'" + employeeFormBean.getFirstName() + "', ");
        values.append("'" + employeeFormBean.getLastName() + "', ");
        values.append("'" + employeeFormBean.getPatronomic() + "', ");
        values.append("'" + employeeFormBean.getBirthday() + "' ,");
        values.append("'" + employeeFormBean.getEmail() + "' ,");
        values.append(employeeFormBean.getSalary() + ",");
        values.append("'" + employeeFormBean.getPhoneNumber() + "',");

        if (employeeFormBean.getIsManager() == null) {

            values.append(employeeFormBean.getDepartmentId() + "");
            addUpdateEntity(qb.buildInsertRequest(values.toString()));
        } else {
            values.append(" NULL ");
            addUpdateEntity(qb.buildInsertRequest(values.toString()));

            qb = new QueryBuilder();
            qb.addTable(TablesNames.EMPLOYEES);
            qb.addWhereParams(qb.setQuartletTwoParams(EmployeesMetadata.EMAIL, QueryBuilder.Sign.EQUAL));
            ArrayList<Employee> emp = getByQueryAndParams(qb.buildSelectRequest(), employeeFormBean.getEmail());


            LinkedHashMap<String, String> map = new LinkedHashMap<>();
            qb = new QueryBuilder();
            qb.addTable(TablesNames.DEPARTMENTS);
            qb.addWhereParams(qb.setQuartletThreeParams(DepartmentsMetadata.DEPARTMENT_ID, QueryBuilder.Sign.EQUAL, employeeFormBean.getDepartmentId() + ""));
            map = new LinkedHashMap<>();
            map.put(DepartmentsMetadata.MANAGER_ID, emp.get(0).getId() + "");
            addUpdateEntity(qb.buildUpdateRequest(map));
        }
    }

    @Override
    public void removeEmployeeFromDepartment(Integer employeeId) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.EMPLOYEES);
        qb.addWhereParams(qb.setQuartletThreeParams(EmployeesMetadata.EMPLOYEE_ID, QueryBuilder.Sign.EQUAL, employeeId + ""));
        LinkedHashMap<String, String> value = new LinkedHashMap<>();
        value.put(EmployeesMetadata.DEPARTMENT_ID, " NULL");
        addUpdateEntity(qb.buildUpdateRequest(value));
    }

    @Override
    public void removeManagerFromDepartment(Integer employeeId) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.DEPARTMENTS);
        qb.addWhereParams(qb.setQuartletThreeParams(DepartmentsMetadata.MANAGER_ID, QueryBuilder.Sign.EQUAL, employeeId + ""));
        LinkedHashMap<String, String> value = new LinkedHashMap<>();
        value.put(DepartmentsMetadata.MANAGER_ID, " NULL");
        addUpdateEntity(qb.buildUpdateRequest(value));
    }


    @Override
    protected Employee getEntity(ResultSet rs) throws SQLException {

        Employee employee = new Employee();

        employee.setId(rs.getInt(EmployeesMetadata.EMPLOYEE_ID_WITHOUT));
        employee.setFirstName(rs.getString(EmployeesMetadata.FIRST_NAME_WITHOUT));
        employee.setLastName(rs.getString(EmployeesMetadata.LAST_NAME_WITHOUT));
        employee.setPatronomic(rs.getString(EmployeesMetadata.PATRONOMIC_WITHOUT));
        employee.setEmail(rs.getString(EmployeesMetadata.EMAIL_WITHOUT));
        employee.setPhoneNumber(rs.getString(EmployeesMetadata.PHONE_NUMBER_WITHOUT));
        employee.setBirthday(rs.getDate(EmployeesMetadata.BIRTHDAY_WITHOUT));
        employee.setSalary(rs.getFloat(EmployeesMetadata.SALARY_WITHOUT));

        try {
            if (employee.getId() == rs.getInt("manager_id")) {
                employee.setIsManager((rs.getString(DepartmentsMetadata.DEPARTMENT_NAME_WITHOUT)));
            } else {
                employee.getDepartment().setId(rs.getInt(EmployeesMetadata.DEPARTMENT_ID_WITHOUT));
                employee.getDepartment().setDepartmentName(rs.getString(DepartmentsMetadata.DEPARTMENT_NAME_WITHOUT));
            }

        } catch (SQLException e) {

        }
        try {
            employee.getDepartment().setId(rs.getInt("departmentId"));
            employee.getDepartment().setDepartmentName(rs.getString("departmentName"));
        } catch (SQLException e) {

        }

        try {

            employee.setIsManager(rs.getString("managerDepName"));
            employee.setDepartmentManagerId(rs.getInt("managerDepId"));

        } catch (SQLException e) {

        }

        return employee;
    }

    @Override
    public LinkedHashMap<String, Object> entityToLinkedHashMap(Employee entity) {
        return null;
    }

}
