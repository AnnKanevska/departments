package database.dao.impl;

import bean.DepartmentFormBean;
import database.dao.DepartmentDao;
import database.dao.util.QueryBuilder;
import entity.Department;
import metadata.DBMetadata.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class DepartmentDaoImpl extends BaseDaoWrapper<Department> implements DepartmentDao {


    //SELECT * FROM locations,countries,departments LEFT JOIN employees On departments.manager_id = employees.employee_id
    // WHERE departments.location_id = locations.location_id AND locations.country_id= countries.country_id;
    private void createQueryForAllDepartments(QueryBuilder qb) {
        qb.addTable(TablesNames.COUNTRIES, TablesNames.LOCATIONS, TablesNames.DEPARTMENTS);
        qb.addWhereParams(qb.setQuartletFourParams(CountriesMetadata.COUNTRY_ID, QueryBuilder.Sign.EQUAL, LocationsMetadata.COUNTRY_ID, QueryBuilder.Operators.AND),
                qb.setQuartletThreeParams(LocationsMetadata.LOCATION_ID, QueryBuilder.Sign.EQUAL, DepartmentsMetadata.LOCATION_ID));
        qb.addLeftJoinParams(TablesNames.EMPLOYEES, DepartmentsMetadata.MANAGER_ID, EmployeesMetadata.EMPLOYEE_ID);
    }

    public ArrayList<Department> getAllDepartments() {
        QueryBuilder qb = new QueryBuilder();
        createQueryForAllDepartments(qb);
        return getByQuery(qb.buildSelectRequest());
    }

    @Override
    public void removeManagerByDepartmentId(Integer departmentId) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.DEPARTMENTS);
        qb.addWhereParams(qb.setQuartletThreeParams(DepartmentsMetadata.DEPARTMENT_ID, QueryBuilder.Sign.EQUAL, departmentId + ""));
        LinkedHashMap<String, String> value = new LinkedHashMap<>();
        value.put(DepartmentsMetadata.MANAGER_ID, " NULL");
        addUpdateEntity(qb.buildUpdateRequest(value));
    }

    @Override
    public boolean isExistDepartmentWithName(String name) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.DEPARTMENTS);
        qb.addWhereParams(qb.setQuartletTwoParams(DepartmentsMetadata.DEPARTMENT_NAME, QueryBuilder.Sign.EQUAL));
        return isExistByQueryAndParams(qb.buildSelectRequest(), name);
    }


    @Override
    public boolean isExistCountry(String country) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.COUNTRIES);
        qb.addWhereParams(qb.setQuartletTwoParams(CountriesMetadata.COUNTRY_NAME, QueryBuilder.Sign.EQUAL));
        return isExistByQueryAndParams(qb.buildSelectRequest(), country);
    }

    @Override
    public Integer getExistCountryId(String country) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.COUNTRIES);
        qb.addWhereParams(qb.setQuartletTwoParams(CountriesMetadata.COUNTRY_NAME, QueryBuilder.Sign.EQUAL));
        System.out.println("Country Exist");
        return getIdByQueryAndParams(CountriesMetadata.COUNTRY_ID, qb.buildSelectRequest(), country);
    }

    @Override
    public boolean isExistLocation(String city, String address) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.LOCATIONS);
        qb.addWhereParams(qb.setQuartletThreeParams(LocationsMetadata.CITY, QueryBuilder.Sign.EQUAL, QueryBuilder.Operators.AND),
                qb.setQuartletTwoParams(LocationsMetadata.ADDRESS, QueryBuilder.Sign.EQUAL));

        return isExistByQueryAndParams(qb.buildSelectRequest(), city, address);
    }

    @Override
    public Integer getExistLocationId(String city, String address) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.LOCATIONS);
        qb.addWhereParams(qb.setQuartletThreeParams(LocationsMetadata.CITY, QueryBuilder.Sign.EQUAL, QueryBuilder.Operators.AND),
                qb.setQuartletTwoParams(LocationsMetadata.ADDRESS, QueryBuilder.Sign.EQUAL));

        return getIdByQueryAndParams(LocationsMetadata.LOCATION_ID, qb.buildSelectRequest(), city, address);
    }

    @Override
    public void addCountry(DepartmentFormBean departmentFormBean) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.COUNTRIES);
        qb.addColumn(CountriesMetadata.COUNTRY_NAME);
        addUpdateEntity(qb.buildInsertRequest("'" + departmentFormBean.getCountry() + "'"));
    }

    @Override
    public void addLocation(DepartmentFormBean departmentFormBean) {
        Integer countryId = null;
        if (isExistCountry(departmentFormBean.getCountry())) {
            countryId = getExistCountryId(departmentFormBean.getCountry());
        } else {
            addCountry(departmentFormBean);
            countryId = getExistCountryId(departmentFormBean.getCountry());
        }
        StringBuffer sb = new StringBuffer();
        sb.append("'").append(departmentFormBean.getCity()).append("', '").append(departmentFormBean.getAddress()).append("', ").append(countryId);
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.LOCATIONS);
        qb.addColumn(LocationsMetadata.CITY, LocationsMetadata.ADDRESS, LocationsMetadata.COUNTRY_ID);
        addUpdateEntity(qb.buildInsertRequest(sb.toString()));

    }

    @Override
    public void addDepartment(DepartmentFormBean departmentFormBean) {
        Integer locationId = null;
        if (isExistLocation(departmentFormBean.getCity(), departmentFormBean.getAddress())) {
            locationId = getExistLocationId(departmentFormBean.getCity(), departmentFormBean.getAddress());
        } else {
            addLocation(departmentFormBean);
            locationId = getExistLocationId(departmentFormBean.getCity(), departmentFormBean.getAddress());
        }
        StringBuffer sb = new StringBuffer();
        sb.append("'").append(departmentFormBean.getName())
                .append("', '").append(departmentFormBean.getDescription()).append("', ").append(locationId).append(",").append(departmentFormBean.getManagerId());

        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.DEPARTMENTS);
        qb.addColumn(DepartmentsMetadata.DEPARTMENT_NAME, DepartmentsMetadata.DESCRIPTION, DepartmentsMetadata.LOCATION_ID, DepartmentsMetadata.MANAGER_ID);
        addUpdateEntity(qb.buildInsertRequest(sb.toString()));

        if (departmentFormBean.getManagerId() != null && !departmentFormBean.getManagerId().equals("null")) {
            qb = new QueryBuilder();
            qb.addTable(TablesNames.EMPLOYEES);

            qb.addWhereParams(qb.setQuartletThreeParams(EmployeesMetadata.EMPLOYEE_ID, QueryBuilder.Sign.EQUAL, departmentFormBean.getManagerId() + ""));
            LinkedHashMap<String, String> value = new LinkedHashMap<>();
            value.put(EmployeesMetadata.DEPARTMENT_ID, " NULL");
            addUpdateEntity(qb.buildUpdateRequest(value));

        }
    }

    @Override
    public void updateDepartment(DepartmentFormBean departmentFormBean) {
        Integer locationId = null;
        if (isExistLocation(departmentFormBean.getCity(), departmentFormBean.getAddress())) {
            locationId = getExistLocationId(departmentFormBean.getCity(), departmentFormBean.getAddress());
        } else {
            addLocation(departmentFormBean);
            locationId = getExistLocationId(departmentFormBean.getCity(), departmentFormBean.getAddress());
        }

        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.DEPARTMENTS);
        qb.addWhereParams(qb.setQuartletThreeParams(DepartmentsMetadata.DEPARTMENT_ID, QueryBuilder.Sign.EQUAL, departmentFormBean.getId() + ""));
        LinkedHashMap<String, String> values = new LinkedHashMap<>();
        values.put(DepartmentsMetadata.DEPARTMENT_NAME, " '" + departmentFormBean.getName() + "' ");
        values.put(DepartmentsMetadata.DESCRIPTION, " '" + departmentFormBean.getDescription() + "' ");
        values.put(DepartmentsMetadata.MANAGER_ID, departmentFormBean.getManagerId() + " ");
        values.put(DepartmentsMetadata.LOCATION_ID, locationId + " ");
        addUpdateEntity(qb.buildUpdateRequest(values));

        if (departmentFormBean.getManagerId() != null && !departmentFormBean.getManagerId().equals("null")) {
            qb = new QueryBuilder();
            qb.addTable(TablesNames.EMPLOYEES);
            qb.addWhereParams(qb.setQuartletThreeParams(EmployeesMetadata.EMPLOYEE_ID, QueryBuilder.Sign.EQUAL, departmentFormBean.getManagerId() + ""));
            LinkedHashMap<String, String> value = new LinkedHashMap<>();
            value.put(EmployeesMetadata.DEPARTMENT_ID, " NULL");
            addUpdateEntity(qb.buildUpdateRequest(value));

        }
    }

    @Override
    public void deleteDepartment(Integer departmentId) {
        QueryBuilder qb = new QueryBuilder();
        LinkedHashMap<String, String> value = new LinkedHashMap<>();
        qb.addTable(TablesNames.DEPARTMENTS);
        qb.addWhereParams(qb.setQuartletThreeParams(DepartmentsMetadata.DEPARTMENT_ID, QueryBuilder.Sign.EQUAL, departmentId + ""));
        value.put(DepartmentsMetadata.MANAGER_ID, " NULL");
        addUpdateEntity(qb.buildUpdateRequest(value));

        qb = new QueryBuilder();
        value = new LinkedHashMap<>();
        qb.addTable(TablesNames.EMPLOYEES);
        qb.addWhereParams(qb.setQuartletThreeParams(EmployeesMetadata.DEPARTMENT_ID, QueryBuilder.Sign.EQUAL, departmentId + ""));
        value.put(EmployeesMetadata.DEPARTMENT_ID, " NULL");
        addUpdateEntity(qb.buildUpdateRequest(value));

        qb = new QueryBuilder();
        qb.addTable(TablesNames.DEPARTMENTS);
        qb.addWhereParams(qb.setQuartletThreeParams(DepartmentsMetadata.DEPARTMENT_ID, QueryBuilder.Sign.EQUAL, departmentId + ""));
        addUpdateEntity(qb.buildDeleteRequest());
    }

    @Override
    public Integer getExistDepartmentId(String name) {
        QueryBuilder qb = new QueryBuilder();
        qb.addTable(TablesNames.DEPARTMENTS);
        qb.addWhereParams(qb.setQuartletTwoParams(DepartmentsMetadata.DEPARTMENT_NAME,QueryBuilder.Sign.EQUAL));

        return getIdByQueryAndParams(DepartmentsMetadata.DEPARTMENT_ID, qb.buildSelectRequest(), name);
    }


    @Override
    protected Department getEntity(ResultSet rs) throws SQLException {

        Department department = new Department();

        department.setId(rs.getInt(DepartmentsMetadata.DEPARTMENT_ID));
        department.setDepartmentName(rs.getString(DepartmentsMetadata.DEPARTMENT_NAME));
        department.setDescription(rs.getString(DepartmentsMetadata.DESCRIPTION));

        department.getLocation().setId(rs.getInt(LocationsMetadata.LOCATION_ID));
        department.getLocation().setAddress(rs.getString(LocationsMetadata.ADDRESS));
        department.getLocation().setCity(rs.getString(LocationsMetadata.CITY));
        department.getLocation().getCountry().setId(rs.getInt(CountriesMetadata.COUNTRY_ID));
        department.getLocation().getCountry().setCountryName(rs.getString(CountriesMetadata.COUNTRY_NAME));

        department.setManagerId(rs.getInt(EmployeesMetadata.EMPLOYEE_ID));
        if(rs.getString(EmployeesMetadata.LAST_NAME)!=null) {
            department.setManagerFullName((rs.getString(EmployeesMetadata.LAST_NAME) + " " + rs.getString(EmployeesMetadata.FIRST_NAME) + " " + rs.getString(EmployeesMetadata.PATRONOMIC)));
        }
        return department;
    }

    @Override
    public LinkedHashMap<String, Object> entityToLinkedHashMap(Department entity) {
        return null;
    }
}
