package database.dao.impl;

import database.connectionFactory.ConnectionFactory;
import database.dao.BaseDao;
import entity.Entity;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public abstract class BaseDaoWrapper<T extends Entity> implements BaseDao<T> {

    private static final Logger LOGGER = Logger.getLogger(BaseDaoWrapper.class);

    public ArrayList<T> getByQuery(String query) {
        ArrayList<T> list = new ArrayList<>();
        try (Connection connection = ConnectionFactory.getInstance().getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query);) {

            while (resultSet.next()) {
                list.add(getEntity(resultSet));
            }
        } catch (SQLException ex) {
            //TODO:Logger and Exception mss
        }
        return list;

    }

    public ArrayList<T> getByQueryAndParams(String query, Object... params) {
        ArrayList<T> list = new ArrayList<>();
        try (Connection connection = ConnectionFactory.getInstance().getConnection();
             PreparedStatement preparedStatement = createPreparedStatement(connection, query, params);
             ResultSet resultSet = preparedStatement.executeQuery();) {

            while (resultSet.next()) {
                list.add(getEntity(resultSet));
            }
        } catch (SQLException ex) {
            //TODO:Logger and Exception mss
        }
        return list;
    }

    public boolean isExistByQueryAndParams(String query, Object... params) {
        try (Connection connection = ConnectionFactory.getInstance().getConnection();
             PreparedStatement preparedStatement = createPreparedStatement(connection, query, params);
             ResultSet resultSet = preparedStatement.executeQuery();) {

            while (resultSet.next()) {
                return true;
            }
        } catch (SQLException ex) {
            //TODO:Logger and Exception mss
        }
        return false;
    }

    public Integer getIdByQueryAndParams(String param, String query, Object... params) {
        try (Connection connection = ConnectionFactory.getInstance().getConnection();
             PreparedStatement preparedStatement = createPreparedStatement(connection, query, params);
             ResultSet resultSet = preparedStatement.executeQuery();) {

            while (resultSet.next()) {
                return resultSet.getInt(param);
            }
        } catch (SQLException ex) {
            //TODO:Logger and Exception mss
        }
        return null;
    }

    private PreparedStatement createPreparedStatement(Connection con, String query, Object... params) throws SQLException {
        PreparedStatement ps = con.prepareStatement(query);
        int count = 1;
        for (Object param : params) {
            ps.setObject(count, param);
            count++;
        }
        return ps;
    }


    public void addUpdateEntity(String query) {
        try (Connection connection = ConnectionFactory.getInstance().getConnection();
             Statement statement = connection.createStatement();) {
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
            //TODO:Logger and Exception mss
        }
    }


    protected abstract T getEntity(ResultSet rs) throws SQLException;

    public abstract LinkedHashMap<String, Object> entityToLinkedHashMap(T entity);
}
