package database.dao;

import bean.DepartmentFormBean;
import entity.Department;

import java.util.ArrayList;

public interface DepartmentDao {

    ArrayList<Department> getAllDepartments();

    public void removeManagerByDepartmentId(Integer departmentId);

    public boolean isExistDepartmentWithName(String name);

    public boolean isExistCountry(String country);

    public Integer getExistCountryId(String country);

    public boolean isExistLocation(String city, String address);

    public Integer getExistLocationId(String city, String address);

    public void addCountry(DepartmentFormBean departmentFormBean);

    public void addLocation(DepartmentFormBean departmentFormBean);

    public void addDepartment(DepartmentFormBean departmentFormBean);

    public void updateDepartment(DepartmentFormBean departmentFormBean);

    public void deleteDepartment(Integer departmentId);

    public Integer getExistDepartmentId(String name);
}
