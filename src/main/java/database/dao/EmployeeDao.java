package database.dao;

import bean.EmployeeFormBean;
import entity.Employee;

import java.util.ArrayList;

public interface EmployeeDao {

    ArrayList<Employee> getAllEmployeesByDepartmentId(Integer departmentId);

    public ArrayList<Employee> getAllEmployees();

    public void deleteEmployeeById(Integer employeeId);

    public void addEmployee(EmployeeFormBean employeeFormBean);

    public boolean isExistEmployeeWithEmail(String email);

    public Integer getExistEmployeeId(String email);

    public void insertEmployee(EmployeeFormBean employeeFormBean);

    public void removeEmployeeFromDepartment(Integer employeeId);

    public void removeManagerFromDepartment(Integer employeeId);
}
