package database.dao.daoFactory;

import database.dao.DepartmentDao;
import database.dao.EmployeeDao;
import database.dao.impl.DepartmentDaoImpl;
import database.dao.impl.EmployeeDaoImpl;

public class MysqlDaoFactory implements DaoFactory {

    @Override
    public DepartmentDao getDepartmentDao() {
        return new DepartmentDaoImpl();
    }

    @Override
    public EmployeeDao getEmployeeDao() {
        return new EmployeeDaoImpl();
    }
}
