package database.connectionFactory;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionFactory {
    //TODO add Logger
    private static ConnectionFactory instance = null;
    private MysqlDataSource dataSource = new MysqlDataSource();


    public static synchronized ConnectionFactory getInstance() /*/throws DBException*/ {
        if (instance == null) {
            instance = new ConnectionFactory();
        }
        return instance;
    }

    private ConnectionFactory() {
        dataSource.setURL("jdbc:mysql://localhost/department");
        dataSource.setUser("root");
        dataSource.setPassword("password");
    }

    public Connection getConnection() {
        Connection con = null;
        try {
            con = dataSource.getConnection();
        } catch (SQLException ex) {

        }
        return con;
    }

}
