package bean;

import javax.servlet.http.HttpServletRequest;

public class EmployeeFormBean {
    Integer id;

    String lastName;

    String firstName;

    String patronomic;

    String email;

    String birthday;

    String phoneNumber;

    String salary;

    Integer departmentId;

    String departmentName;

    String isManager;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronomic() {
        return patronomic;
    }

    public void setPatronomic(String patronomic) {
        this.patronomic = patronomic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getIsManager() {
        return isManager;
    }


    public void setIsManager(String isManager) {
        this.isManager = isManager;
    }

    public static EmployeeFormBean setEmployeeFormBean(HttpServletRequest request) {
        EmployeeFormBean employeeFormBean = new EmployeeFormBean();
        if (request.getParameterMap().containsKey("employeeId")) {
            employeeFormBean.setId(Integer.parseInt(request.getParameter("employeeId").toString()));
        }
        employeeFormBean.setLastName(request.getParameter("lastName").toString());
        employeeFormBean.setFirstName(request.getParameter("firstName").toString());
        employeeFormBean.setPatronomic(request.getParameter("patronomic").toString());

        employeeFormBean.setBirthday((request.getParameter("birthday").toString()));
        employeeFormBean.setEmail(request.getParameter("email").toString());
        employeeFormBean.setPhoneNumber(request.getParameter("phoneNumber").toString());
        employeeFormBean.setSalary((request.getParameter("salary").toString()));

        if (request.getParameterMap().containsKey("departmentName") && request.getParameter("departmentName").toString() != "null") {
            employeeFormBean.setDepartmentId(Integer.parseInt(request.getParameter("departmentId").toString()));
            employeeFormBean.setDepartmentName(request.getParameter("departmentName").toString());
        }
        if (request.getParameterMap().containsKey("IsManager") && request.getParameter("IsManager").toString() != "null") {
            employeeFormBean.setIsManager(request.getParameter("IsManager").toString());
            employeeFormBean.setDepartmentId(Integer.parseInt(request.getParameter("departmentManagerId").toString()));
        }
        if (request.getParameterMap().containsKey("department") && !request.getParameter("department").toString().equals("null")) {
            employeeFormBean.setDepartmentId(Integer.parseInt(request.getParameter("department").toString()));
            employeeFormBean.setDepartmentName("someName");
        }
        if (request.getParameterMap().containsKey("manager") && !request.getParameter("manager").toString().equals("null")) {
            employeeFormBean.setDepartmentId(Integer.parseInt(request.getParameter("manager").toString()));
            employeeFormBean.setIsManager("someName");
        }
        return employeeFormBean;
    }
}
