package bean;

import javax.servlet.http.HttpServletRequest;

public class DepartmentFormBean {

    Integer id;

    String name;

    String country;

    String city;

    String address;

    Integer managerId;

    String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static DepartmentFormBean setDepartmentFormBean(HttpServletRequest request) {
        DepartmentFormBean departmentFormBean = new DepartmentFormBean();
        if (request.getParameterMap().containsKey("departmentId") && request.getParameter("departmentId").toString()!= null && !request.getParameter("departmentId").toString().equals("null")&& !request.getParameter("departmentId").toString().equals("")) {
            System.out.println(request.getParameter("departmentId").toString()+"------------------");
            departmentFormBean.setId(Integer.parseInt(request.getParameter("departmentId").toString()));
        }
        departmentFormBean.setAddress(request.getParameter("address"));
        departmentFormBean.setCity(request.getParameter("city"));
        departmentFormBean.setCountry(request.getParameter("country"));
        departmentFormBean.setDescription(request.getParameter("description"));
        if (request.getParameterMap().containsKey("employeeId") && !request.getParameter("employeeId").equals("null")) {
            departmentFormBean.setManagerId(Integer.parseInt(request.getParameter("employeeId")));
        } else {
            departmentFormBean.setManagerId(null);
        }
        departmentFormBean.setName(request.getParameter("name").toString());

        return departmentFormBean;
    }
}
